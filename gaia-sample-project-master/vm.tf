module "sample_vm_template" {
  source                        = "git::https://adew1:_QcgAFA237wp6vyu-ikM@gitrepo.xlaxiata.id/terraform/modules/gcp/vm.git//modules/instance_template?ref=release-20210721-1"
  region                        = var.gcloud_region
  project_id                    = var.project_id
  subnetwork                    = local.subnetwork_selflink
  name_prefix                   = "${var.machine_name}-sample-vm-tpl"
  machine_type                  = "n1-standard-1"
  tags                          = [ var.project_id , "${var.project_id}-sample_vm" , "${var.project_id}-allow-ssh-internal", "${var.project_id}-allow-es-master-vm"]
  source_image_project          = "ubuntu-os-cloud"
  source_image_family           = "ubuntu-2004-lts"
  source_image                  = "ubuntu-2004-focal-v20210603"
  disk_size_gb                  = 50
  additional_disks              = [
      {
        disk_name    = ""
        device_name  = null
        auto_delete  = true
        boot         = false
        disk_size_gb = 50
        disk_type    = null
      }
  ]
  service_account               = {
    email  = ""
    scopes = []
  }
  

}
module "sample_vm" {
  source            = "git::https://adew1:_QcgAFA237wp6vyu-ikM@gitrepo.xlaxiata.id/terraform/modules/gcp/vm.git//modules/compute_instance?ref=release-20210721-1"
  region            = var.gcloud_region
  network           = local.network_selflink
  subnetwork        = local.subnetwork_selflink
  num_instances     = 1
  hostname          = "${var.machine_name}-es-master-server"
  instance_template = module.sample_vm_template.self_link

  machine_type      = "n1-standard-4"
  service_account   = {
    email  = ""
    scopes = ["cloud-platform"]
  }
  allow_stopping_for_update = true
}

