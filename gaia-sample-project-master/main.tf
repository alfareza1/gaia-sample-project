provider "google"{
  project                             = var.project_id
  region                              = var.gcloud_region
}

locals {
  #anthosdev_master_authorized_network  = var.subnet_range
  subnetwork_selflink 		             = "projects/${var.network_project_id}/regions/${var.gcloud_region}/subnetworks/${var.subnet_name}"
  network_selflink                     = "projects/${var.network_project_id}/global/networks/${var.vpc_name}"
}


data "terraform_remote_state" "state" {
  backend = "gcs"
  config = {
    bucket = "xl-${var.project_name}-${var.environment}-tfstate"
    prefix = ""
  }
}
