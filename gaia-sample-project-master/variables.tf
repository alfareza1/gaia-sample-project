/*===================================================================================================
***************************************** General *****************************************
===================================================================================================*/

variable "project_name" {
  description   = "Project Name"
  default       = "anthos-dev"
}

variable "project_id" {
  description   = "Project ID of GCP"
  default       = "anthos-dev-3ace"
}

variable "network_project_id" {
  description   = "network project ID of GCP"
  default       = ""
}

variable "gcloud_region" {
  description   = "GCP region that the instances will be created"
  default       = "asia-southeast2"
}

variable "environment" {
  description   = "Name of the environment for prod/nonprod"
  default       = "prod"
}

variable "vpc_name" {
  description = "Name of the VPC"
}

variable "subnet_name" {
  description = "Subnet name"
}

// variable "subnet_range" {
//   description = "Subnet range"
// }

variable "machine_name" {
  description = "machine name"
}
