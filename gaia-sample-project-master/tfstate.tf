module "tfstate" {
  source                        = "git::https://adew1:_QcgAFA237wp6vyu-ikM@gitrepo.xlaxiata.id/terraform/modules/gcp/cloud-storage.git//modules/simple_bucket?ref=release-20210427-1"
  
  name                          = "xl-${var.project_name}-${var.environment}-tfstate"
  project_id                    = var.project_id
  location                      = var.gcloud_region
  bucket_policy_only            = false
  storage_class                 = "REGIONAL"
  force_destroy                 = true
  labels                        =  {
      
  }
}
